import {IUser} from './user';

export interface ISettings {
  id: number;
  name: string;
  description: string;
  team: IUser[];
  teamLead: IUser;
}
