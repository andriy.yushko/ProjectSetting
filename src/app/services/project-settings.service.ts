import {Injectable} from '@angular/core';
import {IUser} from '../models/user';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/map';
import {ISettings} from '../models/settings';
import 'rxjs/Rx';
@Injectable()
export class ProjectSettingsService {

  constructor(private _http: Http) {
  }

  getUsers(): Observable<IUser[]> {
    return this._http.get('api/users')
      .map( response => response.json())
      .catch(this.handleErrorObservable);
  }

  saveProjectSettings(settings: ISettings): Promise<ISettings> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this._http.put(`api/settings/1`, JSON.stringify(settings), options).toPromise()
      .then(() => settings)
      .catch(error => this.handleErrorPromise(error));
  }

  // TODO: Will be using for get current settings from server
  getCurrentSettings(): Promise<ISettings> {
    const url = `api/settings`;
    return this._http.get(url)
      .toPromise()
      .then(response => response.json() as ISettings)
      .catch(error => this.handleErrorPromise(error));
  }
  private handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  private handleErrorPromise (error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }
}
