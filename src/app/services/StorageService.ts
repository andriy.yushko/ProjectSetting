import {InMemoryDbService} from 'angular-in-memory-web-api';

export class StorageData implements InMemoryDbService {
  createDb() {
    const users = [
      {
        "name": "Bob",
        "surname": "Bobko",
        "email": "bobkobo@yaware.com"
      },
      {
        "name": "John",
        "surname": "Jhonson",
        "email": "john@yaware.com"
      },
      {
        "name": "Tom",
        "surname": "Brawn",
        "email": "brawn@yaware.com"
      },
      {
        "name": "Tor",
        "surname": "Odison",
        "email": "tor@yaware.com"
      },
      {
        "name": "Oleg",
        "surname": "Vovchok",
        "email": "vovchok2108@yaware.com"
      },
      {
        "name": "Andriy",
        "surname": "Nazarenko",
        "email": "nazar@yaware.com"
      },
      {
        "name": "Some",
        "surname": "One",
        "email": "someone@yaware.com"
      }
    ];

    const settings = [{
      "id": 1,
      "name": "",
      "description": "",
      "team": [ ],
      "teamLead": {
        "name": "",
        "surname": "",
        "email": ""
      }
    }
    ];


    return {settings, users};
  }
}
