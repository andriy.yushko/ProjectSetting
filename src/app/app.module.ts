import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SettingsModalComponent} from './components/project-settings/settings-modal/settings-modal.component';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ProjectSettingsService} from './services/project-settings.service';
import {ProjectSettingsComponent} from './components/project-settings/project-settings.component';
import {HttpModule} from '@angular/http';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {StorageData} from './services/StorageService';
import {FormsModule} from '@angular/forms';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {LoadingModule} from 'ngx-loading';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ShowToastrService} from './components/_show_toastr/show-toastr.service';


@NgModule({
  declarations: [
    AppComponent,
    SettingsModalComponent,
    ProjectSettingsComponent
  ],
  imports: [
    BrowserModule,
    BootstrapModalModule,
    NgbModule.forRoot(),
    HttpModule,
    AngularFontAwesomeModule,
    InMemoryWebApiModule.forRoot(StorageData),
    FormsModule,
    Ng2SearchPipeModule,
    LoadingModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    ProjectSettingsService,
    StorageData,
    ShowToastrService
  ],
  entryComponents: [
    SettingsModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
