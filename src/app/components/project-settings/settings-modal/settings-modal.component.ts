import {Component, ViewContainerRef} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {IUser} from '../../../models/user';
import {ISettings} from '../../../models/settings';
import {ProjectSettingsService} from '../../../services/project-settings.service';
import {ShowToastrService} from '../../_show_toastr/show-toastr.service';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './settings-modal.component.html',
  styleUrls: ['./settings-modal.component.scss']
})
export class SettingsModalComponent extends DialogComponent<{}, null> implements IUser {

  name: string;
  surname: string;
  email: string;
  listAllUsers: IUser[] = [];
  selectedUsers: IUser[] = [];
  currentSettings: any = {};
  searchUserFromAll: string = '';
  searchUserFromSelected: string = '';
  loading = false;
  disabled = false;
  isVisible = true;

  settings: ISettings = {
    id: 1,
    name: 'dfsfdsfsf',
    description: '',
    team: this.selectedUsers,
    teamLead: <IUser> {}
  };

  constructor(private _dialogService: DialogService,
              private _settingsService: ProjectSettingsService,
              private _toastr: ShowToastrService) {
    super(_dialogService);
    this.currentSettings = JSON.parse(localStorage.getItem('settings'));
    this.selectedUsers = this.currentSettings.team;

    this.settings = {
      id: this.currentSettings.id,
      name: this.currentSettings.name,
      description: this.currentSettings.description,
      team: this.currentSettings.team,
      teamLead: this.currentSettings.teamLead
    };
  }

  addIntoSelectedUsers(user: IUser) {
    if (!this.selectedUsers.includes(user)) {
      this.selectedUsers.push(user);
      const index = this.listAllUsers.indexOf(user);
      this.listAllUsers.splice(index, 1);
    }else {
      const index = this.listAllUsers.indexOf(user);
      this.listAllUsers.splice(index, 1);
    }
  }

  removeFromSelectedUsers(user: IUser) {
    if (!this.listAllUsers.includes(user)) {
      this.listAllUsers.push(user);
      const index = this.selectedUsers.indexOf(user);
      this.selectedUsers.splice(index, 1);
    }else {
      const index = this.selectedUsers.indexOf(user);
      this.selectedUsers.splice(index, 1);
    }

  }

  saveProjectSettings() {
    this.settings.id = 1;
    this.loading = true;
    this.disabled = true;
    this._settingsService.saveProjectSettings(this.settings)
      .then(
        settings => {
          localStorage.setItem('settings', JSON.stringify(settings));
          this.loading = false;
          this.isVisible = false;
          this._toastr.sweetNotification('Settings save successfully!', 'success', 3000);
        },
        error => console.log(error));
  }
}
