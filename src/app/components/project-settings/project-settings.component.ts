import {Component, OnInit} from '@angular/core';
import {SettingsModalComponent} from './settings-modal/settings-modal.component';
import {DialogService} from 'ng2-bootstrap-modal';
import {ProjectSettingsService} from '../../services/project-settings.service';
import {IUser} from '../../models/user';

@Component({
  selector: 'app-project-settings',
  templateUrl: './project-settings.component.html',
  styleUrls: ['./project-settings.component.scss']
})
export class ProjectSettingsComponent implements OnInit {

  public listAllUsers: IUser[] = [];
  public currentSettings: any = {};
  constructor(private dialogService: DialogService, private _settingsService: ProjectSettingsService) {
  }

  ngOnInit() {
    this._settingsService.getUsers().subscribe(response =>
      this.listAllUsers = response, error => console.log(error)
    );
  }

  showModal() {
    this.dialogService.addDialog(SettingsModalComponent, {
      listAllUsers: this.listAllUsers
    }, {backdropColor: 'rgba(0, 0, 0, 0.5)'});
  }
}
