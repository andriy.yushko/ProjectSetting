import { TestBed, inject } from '@angular/core/testing';

import { ShowToastService } from './show-toastr.service';

describe('ShowToastService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowToastService]
    });
  });

  it('should ...', inject([ShowToastService], (service: ShowToastService) => {
    expect(service).toBeTruthy();
  }));
});
