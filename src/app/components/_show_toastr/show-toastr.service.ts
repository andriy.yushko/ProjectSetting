import { Injectable } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import swal from 'sweetalert2';

@Injectable()
export class ShowToastrService {

  constructor(public toastr: ToastsManager) {}

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message);
  }

  showWarning(message: string) {
    this.toastr.warning(message);
  }

  sweetNotification(message, type, time) {
    swal({
      text: message,
      type: type,
      confirmButtonColor: '#3085d6',
      confirmButtonClass: 'btn btn-success',
      buttonsStyling: true,
      timer: time
    }).catch(swal.noop);
  }
}
